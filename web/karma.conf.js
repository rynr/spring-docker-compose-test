// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

const { Builder } = require("selenium-webdriver");

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', '@angular-devkit/build-angular'],
    plugins: [
      require('karma-jasmine'),
      require('karma-chrome-launcher'),
      require('karma-jasmine-html-reporter'),
      require('karma-coverage-istanbul-reporter'),
      require('@angular-devkit/build-angular/plugins/karma')
    ],
    client: {
      clearContext: false // leave Jasmine Spec Runner output visible in browser
    },
    coverageIstanbulReporter: {
      dir: require('path').join(__dirname, './coverage/remote-games'),
      reports: ['html', 'lcovonly', 'text-summary'],
      fixWebpackSourcePaths: true
    },
    reporters: ['progress', 'kjhtml'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['swd_firefox', 'swd_chrome', 'swd_chrome'],
    singleRun: false,
    restartOnFileChange: true,
    // define browsers
    customLaunchers: {
      swd_firefox: {
        base: 'SeleniumWebdriver',
        browserName: 'Firefox',
        getDriver: function(){
          return new Builder()
              .usingServer("http://firefox:4444")
              .build();
        }
      },
      swd_chrome: {
        base: 'SeleniumWebdriver',
        browserName: 'Chrome',
        getDriver: function(){
          return new Builder()
              .usingServer("http://chrome:4444")
              .build();
        }
      },
      swd_opera: {
        base: 'SeleniumWebdriver',
        browserName: 'Opera',
        getDriver: function(){
          return new Builder()
              .usingServer("http://opera:4444")
              .build();
        }
      }
    }
  });
};
